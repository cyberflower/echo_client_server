#include <stdio.h> // for perror
#include <iostream>
#include <stdlib.h> // for exit
#include <string.h> // for memset
#include <unistd.h> // for close
#include <arpa/inet.h> // for htons
#include <netinet/in.h> // for sockaddr_in
#include <sys/socket.h> // for socket
#include <thread>
#include <deque>
#include <mutex>
#include <signal.h> // for keyboard interrupt

using namespace std;
const int SZ=1005; // maximum size of thread

thread client[SZ];
int child_exist[SZ];
int visit[SZ];
deque<int> unused_thread_idx;

void usage(){
    printf("syntax : echo_server <port> [-b]\n");
    printf("sample : echo_server 1234 -b\n");
}

void terminate(int sig){
	printf("\n[!] Close server\n");
	for(int i=1;i<SZ;i++){
		char *end_message="quit";
		if(child_exist[i]!=-1) send(child_exist[i],end_message,strlen(end_message),0);
	}	
	exit(0);
}

void recv_send(int sockfd, int childfd, bool broad_cast, int *child_exist, int idx, mutex &m){
	while(true){
		const static int BUFSIZE = 1024;
		char buf[BUFSIZE];

		ssize_t received = recv(childfd, buf, BUFSIZE - 1, 0);
		if (received == 0 || received == -1) {
			m.lock();
			printf("[!] Process %d terminated\n",idx);
			child_exist[idx]=-1;
			m.unlock();
			return;
		}
		buf[received] = '\0';
		printf("%s\n", buf);
		if(strcmp(buf,"quit")==0){
			m.lock();
			printf("[!] Process %d terminated\n",idx);
			child_exist[idx]=-1;
			m.unlock();
			return;
		}
		if(!broad_cast){
			ssize_t sent = send(childfd, buf, strlen(buf), 0);
			m.lock();
			if (sent == 0) {
				printf("[!] Process %d terminated\n",idx);
				child_exist[idx]=-1;
				return;
			}
			m.unlock();		
		}
		else{
			for(int i=0;i<SZ;i++){
				if(child_exist[i]==-1) continue;
				ssize_t sent = send(child_exist[i], buf, strlen(buf),0);
				m.lock();
				if(sent==0){
					perror("send failed");
					child_exist[idx]=-1;
					return;				
				}			
				m.unlock();
			}
		}	
	}
}

int main(int argc, char **argv){
	bool broad_cast=false;
    if(argc==2 || argc==3){
		if(argc==3 && strcmp("-b",argv[2])==0){
			cout<<"broad cast on"<<'\n';
			broad_cast=true;
		}
		else if(argc==2) broad_cast=false;
		else{
			usage();
			return -1;
		}
	}
	else{
		usage();
		return -1;
	}

	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1) {
		perror("socket failed");
		return -1;
	}

	int optval = 1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,  &optval , sizeof(int));

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(atoi(argv[1]));
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	memset(addr.sin_zero, 0, sizeof(addr.sin_zero));
	for(int i=1;i<SZ;i++) unused_thread_idx.push_front(i);
	memset(child_exist,-1,sizeof(child_exist));
	int res = bind(sockfd, reinterpret_cast<struct sockaddr*>(&addr), sizeof(struct sockaddr));
	if (res == -1) {
		perror("bind failed");
		return -1;
	}

	res = listen(sockfd, 2);
	if (res == -1) {
		perror("listen failed");
		return -1;
	}
	for(int i=1;i<SZ;i++) unused_thread_idx.push_back(i);
	int idx=0;
	mutex m;
	signal(SIGINT, terminate);
	while (!unused_thread_idx.empty()) {
		struct sockaddr_in addr;
		socklen_t clientlen = sizeof(sockaddr);
		int childfd = accept(sockfd, reinterpret_cast<struct sockaddr*>(&addr), &clientlen);
		
		if (childfd < 0) {
			perror("ERROR on accept");
			break;
		}
		printf("connected\n");
		int idx=unused_thread_idx.front(); unused_thread_idx.pop_front(); 
		child_exist[idx]=childfd;
		client[idx]=thread(recv_send,sockfd,childfd,broad_cast,child_exist,idx,ref(m));
		visit[idx]=1;
		unused_thread_idx.push_back(idx);
	}
	for(int i=1;i<SZ;i++){
		if(visit[i]) client[idx].join();
	}
	close(sockfd);
    return 0;
}
